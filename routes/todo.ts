import { Router } from "../deps.ts"

const router = new Router()
// controller
import todoController from "../controllers/todo.ts"

router
  .get("/", (ctx) => {
    ctx.response.body = {
      code: 200,
      msg: `Ok`,
      data: {}
    }
  })
  .get("/todos", todoController.getAllTodos)
  .post("/todos", todoController.createTodo)
  .get("/todos/:id", todoController.getTodoById)
  .put("/todos/:id", todoController.updateTodoById)
  .delete("/todos/:id", todoController.deleteTodoById)

export default router
