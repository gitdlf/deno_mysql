// 定义数据库
export const DATABASE: string = (
  "deno_example" + new Date().toLocaleDateString()
).replace(/\//g, "_")

// 定义数据表
export const TABLE = {
  TODO: "todo"
}
