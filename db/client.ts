// 从mysql库中导入Client 帮助我们连接到我们的数据库并在数据库中执行操作
import { Client } from "../deps.ts"
// config
import { DATABASE, TABLE } from "./config.ts"

const client = await new Client()

// # 建立MySQL 实例连接

// Client提供了一个名为的方法connect，该方法接受对象，提供hostname, username,password和db
client.connect({
  hostname: "localhost", // 或 "127.0.0.1"
  // port: 3306, // 默认
  username: "root",
  password: "",
  // poolSize: 3, // connection limit
  // 链接mysql已有的数据库<代码创建数据库的话，此时不填，填了反正也不是去它里面加减数据>
  db: ""
})

const run = async () => {
  // 创建库 (如果不存在此数据库)
  await client.execute(`CREATE DATABASE IF NOT EXISTS ${DATABASE}`)
  // 选择使用库
  await client.execute(`USE ${DATABASE}`)

  // // 删除表 （如果存在就删除）
  // await client.execute(`DROP TABLE IF EXISTS ${TABLE.TODO}`)

  // 创建表  （如果不存在就创建）
  await client.execute(`
    CREATE TABLE IF NOT EXISTS ${TABLE.TODO} (
        id int(11) NOT NULL AUTO_INCREMENT,
        todo varchar(100) NOT NULL,
        isCompleted boolean NOT NULL default false,
        PRIMARY KEY (id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  `)

  // // 测试读写数据表的返回值
  // const result = await client.query(
  //   // `INSERT INTO deno_example2022_6_17.todo(todo, isCompleted) values("gg2", true)`  // { affectedRows: 1, lastInsertId: 4 }
  //   `select * from deno_example2022_6_17.todo` // [{},{}]
  // )
  // console.log("result", result.count, typeof result)
}

run()

export default client
