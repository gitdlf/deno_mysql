import { Application } from "./deps.ts"
import { green, yellow } from "./deps.ts"

// routes
import todoRouter from "./routes/todo.ts"
// logger
import logger from "./middlewares/logger.ts"
// not found
import notFound from "./middlewares/notFound.ts"

const app = new Application()
const port: number = 8765

// order of execution is important;
app.use(logger.logger)
app.use(logger.responseTime)

app.use(todoRouter.routes())
app.use(todoRouter.allowedMethods())

// 404 page
app.use(notFound)

app.addEventListener("listen", ({ secure, hostname, port }) => {
  const protocol = secure ? "https://" : "http://"
  const hostName = hostname === "0.0.0.0" ? "localhost" : hostname

  const url = `${protocol}${hostName ?? "localhost"}:${port}`

  console.log(`${yellow("Listening on:")} ${green(url)}`)
})

await app.listen({ port })
