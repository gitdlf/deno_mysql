export default interface Todo {
  // ?使对象中的键成为可选
  id?: number;
  todo?: string;
  isCompleted?: boolean;
}
